<h1>Contexte</h1>

Ce projet a été réalisé dans le cadre d'une première année de DUT informatique à l'IUT de Montpellier. Il a pour but d'initier les étudiants à la programmation orientée objets.

L'othello est un jeu de plateau dans lequel le but est de gagner un maximum de pions. Pour cela, chaque joueur pose tour à tour un pion de sa couleur sur le plateau, de façon à encadrer ceux de la couleur adverse. Lorsque les deux extrémités d'une ligne de pions blancs sont stoppées par des pions noirs, le joueur ayant les pions noirs remporte cette ligne, et celle-ci change donc de couleur.

Vous trouverez une liste plus complète des règles en suivant <a href='https://www.regledujeu.fr/othello/'>ce lien</a>.

<h1>Principe</h1>

- Il est possible de faire une partie à deux joueurs en local.

- Il est possible de jouer contre l'ordinateur sur trois niveaux de difficulté
    - Facile : les pions de l'ordinateur sont placés au hasard sur le plateau.
    - Moyen : les pions de l'ordinateur sont placés de façon à remporter le plus de pions possibles à chaque tour.
    - Difficile : les pions de l'ordinateur sont placés à des positions stratégiques.

- Le score final de chaque partie est sauvegardé, permettant de voir un classement local.

<h1>Code</h1>

- Codé en Java.

- Utilise la librairie <a href='http://hsqldb.org/'>HSQLDB</a> afin de sauvegarder les scores sur une base de données interne locale.

- Utilise la librairie <a href='https://openjfx.io/'>JavaFX</a> pour implémenter l'interface graphique.

- Contient des sons donnant des informations sur le placement d'un pion.
    - Lorsqu'un pion est placé sur une case stratégiquement intéressante.
    - Lorsqu'un placement rapporte beaucoup de pions.
    - Lorsqu'un joueur tente de placer un pion sur une case où il ne peut pas.

- Dans une partie de niveau difficile, l'ordinateur choisit son placement en fonction d'un poids indiqué sur chaque case en fonction de différentes stratégies.
